<!-- 404.php -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ja">
<head>
<meta http-equiv="refresh" content="2;URL=http://www.ivisit.co.jp/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ページが見つかりませんでした - 営業支援/人材派遣アウトソーシングはアイヴィジット</title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="Keywords" content="営業支援,人材派遣,セールスプロモーション,訪問サービス,アウトソーシング" />
<meta name="Description" content="株式会社アイヴィジットは営業支援・人材派遣企業を官公庁・自治体・百貨店・大手量販店・個人宅といった様々なフィールドオペレーションに支援サービスをご提供します" />
<link rel="stylesheet" type="text/css" href="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/css/import.css" />
<link rel="shortcut icon" href="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/favicon.ico" type="image/vnd.microsoft.icon">
<script type="text/javascript" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/js/jquery.carouFredSel-5.5.5-packed.js"></script>
<script type="text/javascript" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/js/jquery.cslider.js"></script>
<script type="text/javascript" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/js/common.js"></script>
<!-- IE8以下用 -->
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.ivisit.co.jp/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.ivisit.co.jp/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 3.5.1" />
</head>
<body id="pageTop">
<div id="wrapper">


<!-- //▼HEADER▼// -->
<div id="header">
<div class="frame">

<div id="headTop" class="clearfix">
<h1>フィールドオペレーションアウトソーシング企業　株式会社アイヴィジット</h1>
<ul id="headMenu" class="clearfix">
	<li><a href="?page_id=91"><img alt="サイトマップ" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/hm_sitemap.png" width="72" height="10" class="over" /></a></li>
	<li><a href="?page_id=69"><img alt="個人情報保護方針" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/hm_privacy.png" width="92" height="10" class="over" /></a></li>
</ul>
<p id="tell"><img alt="電話番号" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/hm_tell.png" width="208" height="36" /></p>
</div>

<div id="headBtm" class="clearfix">
<p id="logo"><a href="http://www.ivisit.co.jp"><img alt="株式会社アイヴィジット" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/logo.png" width="115" height="80" class="over" /></a></p>
<ul id="gnavi" class="clearfix">
	<li><a href="http://www.ivisit.co.jp"><img alt="ホーム" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/gn_home.png" width="160" height="40" class="imgover" /></a></li>
	<li><a href="?page_id=38"><img alt="会社案内" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/gn_about.png" width="160" height="40" class="imgover" /></a></li>
	<li><a href="?page_id=30"><img alt="事業内容" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/gn_business.png" width="160" height="40" class="imgover" /></a></li>
	<!--{*<li><a href="#"><img alt="求人情報" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/gn_recruit.png" width="160" height="40" class="imgover" /></a></li>*}-->
	<li><a href="http://www.ivisit.co.jp/client/index.php"><img alt="お問い合わせ" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/gn_contact.png" width="159" height="40" class="imgover" /></a></li>
	<li><a href="http://jobvisit.net/" target="_blank"><img alt="販売・官公庁のアルバイト・バイト求人ならジョブヴィジット" src="http://www.ivisit.co.jp/wp-content/themes/themIvisit/common/img/gn_jobvisit.png" width="158" height="40" class="over" /></a></li>
</ul>
</div>

</div>
</div>
<!-- //△HEADER△// -->



<div id="container">
<div class="frame clearfix">


<!-- //▼MAIN▼// -->
<div id="mainArea">
	<div style="margin:100px 0 0 350px;">
<h2>指定されたページは存在しませんでした。</h2>
<p>このページの URL ：
<span class="error_msg">
http://<?php echo esc_html($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']); ?>
</span></p>
<p>現在表示する記事がありません。</p>
<p><a href="<?php echo home_url(); ?>">トップページへ戻る</a></p> 
</div>
</div>

</div>
</div>
<!-- //△CONTAINER△// -->



<?php get_footer(); ?>
