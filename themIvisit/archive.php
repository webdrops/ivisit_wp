<?php
/*
Template Name: お知らせ一覧用テンプレート
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#article: http://ogp.me/ns/article#">
<title>お知らせ一覧 - 営業支援/人材派遣アウトソーシングはアイヴィジット</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="株式会社アイヴィジットは営業支援・人材派遣企業を官公庁・自治体・百貨店・大手量販店・個人宅といった様々なフィールドオペレーションに支援サービスをご提供します">
<meta name="keyword" content="営業支援,人材派遣,セールスプロモーション,訪問サービス,アウトソーシング">
<link rel="shortcut icon" href="<?php home_url(); ?>/favicon.ico">
<link rel="stylesheet" href="/assets/css/master.css">
<link rel="stylesheet" href="/assets/css/slick.css">
<link rel="stylesheet" href="/assets/css/swiper.min.css">
<script src="/assets/js/vendor/jquery.1.11.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHLWZ8C');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div id="l-wrapper">
  <?php get_header(); ?>
  <article>
    <div class="c-pageTtl_wrapper">
      <h1 class="c-pageTtl c-pageTtl--information">
        <span class="c-pageTtl_ttl">お知らせ</span>
        <span class="c-pageTtl_en">News Release</span>
      </h1>
    </div>
    <div class="l-contents">
      <div class="l-sec02 u-mb40_sp">
        <div class="l-wrap l-wrap--small">
          <div class="c-orangeBox c-orangeBox-padSideSmall">
            <p class="c-orangeBox_ttl">お知らせ</p>
            <ul class="c-yearList">
            <?php
                $args = array(
                  "post_type" => "post",
                  "posts_per_page" => -1,
                  "post_status" => "publish",
                  "orderby "=> "date",
                );
                $query = new WP_Query($args);
                $postYear = array();
              ?>
              <?php if ($query->have_posts()): ?>
              <?php while ($query->have_posts()) : $query->the_post(); ?>
              <?php array_push($postYear,get_the_date('Y'));?>
              <?php endwhile; ?>
              <?php endif; ?>
              <?php
                $postYearList = array_unique($postYear);
                if(!empty($postYearList)){
                  foreach($postYearList as $value){
                    echo '<li><a href="#'.$value.'">'.$value.'年</a></li>';;
                  }
                }
              ?>
            </ul>
          </div>
        </div>
      </div>

      <?php
        $args = array(
          "post_type" => "post",
          "posts_per_page" => -1,
          "post_status" => "publish",
          "orderby "=> "date",
        );
        $query = new WP_Query($args);
        $postYear = array();
      ?>
      <?php if ($query->have_posts()): ?>
      <?php while ($query->have_posts()) : $query->the_post(); ?>
      <?php array_push($postYear,get_the_date('Y'));?>
      <?php endwhile; ?>
      <?php endif; ?>
      <?php
        $postYearList = array_unique($postYear);
        if(!empty($postYearList)){
          $html = "";
          foreach($postYearList as $value){

            $html .= '<section class="l-sec02" id="'.$value.'">';
            $html .= '<div class="l-wrap"><h2 class="c-secTtl03">'.$value.'年</h2>';
            $html .= '<ul class="c-newsList">';

            $postArgs = array(
              "post_type" => "post",
              "posts_per_page" => -1,
              "post_status" => "publish",
              "year" => $value,
              "orderby "=> "date",
            );
            $postQuery = new WP_Query($postArgs);
            if ($postQuery->have_posts()){
              while ($postQuery->have_posts()){
                $postQuery->the_post();
                $postID = $postQuery->post->ID;
                $category = get_the_category($postID);
                $categoryName = $category[0]->name;

                $html .= '<li class="c-newsList_item">';
                $html .= '<a href="'.get_the_permalink($postID).'">';
                $html .= '<p class="c-newsList_item_date">'.get_the_time('Y/m/d').'</p>';
                if($categoryName === "人事"){
                  $html .= '<p class="c-label">'.$categoryName.'</p>';
                }elseif($categoryName === "入札"){
                  $html .= '<p class="c-label c-label--red">'.$categoryName.'</p>';
                }elseif($categoryName === "受託"){
                  $html .= '<p class="c-label c-label--blue">'.$categoryName.'</p>';
                }elseif($categoryName === "その他"){
                  $html .= '<p class="c-label c-label--gray">'.$categoryName.'</p>';
                }
                $html .= '<p class="c-newsList_item_txt">'.get_the_title($postID).'</p>';
                $html .= '</a>';
                $html .= '</li>';

              }
            }
            $html .= '</ul></div></section>';

          }
          echo $html;
        }
      ?>
    </div>
  </article>
  <?php get_footer(); ?>
</div>
<script src="/assets/js/vendor/jquery.matchHeight.js"></script>
<script src="/assets/js/vendor/picturefill.min.js"></script>
<script src="/assets/js/vendor/ofi.min.js"></script>
<script src="/assets/js/vendor/slick.min.js"></script>
<script src="/assets/js/vendor/swiper.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
