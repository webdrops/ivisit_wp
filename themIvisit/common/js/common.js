/* ===============================================
# トップニュース
=============================================== */
$(function() {
		$("#topReason ol li:nth-child(3n)").css('border-right', 'none');
		$("#topReason ol li").slice(3).css('border-bottom', 'none');
		$("#sideArea #foothold #sfOffice li:last-child").css('border-bottom', 'none');
		$("#sideArea #sideMenu ul li:last-child").css('border-bottom', 'none');
		$("#footer #footMenu .fmBox").slice(4).css('margin', '0');
		$("#foothold .textBox .fhBox:last-child").css('border-bottom', 'none');
		$("#topics .topicsList ul li:last-child").css('border-bottom', 'none');
});

/* ===============================================
# 導入サービス
=============================================== */
$(function() {
	$('#hgslide').carouFredSel({
		circular: true,
		infinite: false,
		scroll: 1,
		prev: '#prev2',
		next: '#next2',
		auto: true
	});
});

/* ===============================================
# class="imgover" の要素に、マウスオーバーで
　"_o.gif" の画像と入れ替える
=============================================== */
function initRollovers() {
	if (!document.getElementById) return
	
	var aPreLoad = new Array();
	var sTempSrc;
	var aImages = document.getElementsByTagName('img');

	for (var i = 0; i < aImages.length; i++) {		
		if (aImages[i].className == 'imgover') {
			var src = aImages[i].getAttribute('src');
			var ftype = src.substring(src.lastIndexOf('.'), src.length);
			var hsrc = src.replace(ftype, '_o'+ftype);

			aImages[i].setAttribute('hsrc', hsrc);
			
			aPreLoad[i] = new Image();
			aPreLoad[i].src = hsrc;
			
			aImages[i].onmouseover = function() {
				sTempSrc = this.getAttribute('src');
				this.setAttribute('src', this.getAttribute('hsrc'));
			}	
			
			aImages[i].onmouseout = function() {
				if (!sTempSrc) sTempSrc = this.getAttribute('src').replace('_o'+ftype, ftype);
				this.setAttribute('src', sTempSrc);
			}
		}
	}
}
window.onload = initRollovers;
try{
	window.addEventListener("load",initRollovers,false);
}catch(e){
	window.attachEvent("onload",initRollovers);
}
