<footer class="l-footer">
    <p class="l-footer_pageTop"><a href="#" class="js-pageTop"><img src="/assets/images/common/page-top.jpg" alt="PageTop"></a></p>
    <p class="l-footer_spBtn"><a href="/nenkin/">アイヴィジットの年金サイト</a></p>
    <div class="l-footer_inr">
      <div class="l-footer_links">
        <p class="l-footer_links_menu_ttl"><a href="/">TOPページ</a></p>
        <div class="l-footer_links_menuUnit">
          <div class="l-footer_links_menu">
            <p class="l-footer_links_menu_ttl"><a href="/company/index.html">会社案内</a></p>
            <ul class="l-footer_links_menu_list">
              <li><a href="/company/about.html">会社概要</a></li>
              <li><a href="/company/about.html#history">沿革</a></li>
              <li><a href="/company/about.html#group">グループ企業</a></li>
              <li><a href="/company/branch.html">拠点案内</a></li>
              <li><a href="/philosophy.html">企業理念・品質管理方針</a></li>
            </ul>
          </div>
          <div class="l-footer_links_menu">
            <p class="l-footer_links_menu_ttl"><a href="/work/index.html">事業内容</a></p>
            <ul class="l-footer_links_menu_list">
              <li><a href="/work/event.html">イベント企画・運営</a></li>
              <li><a href="/work/round.html">店舗・販路ラウンド</a></li>
              <li><a href="/work/varioussurveys.html">各種調査代行・支援</a></li>
              <li><a href="/work/administration/index.html">行政サービス支援</a></li>
            </ul>
          </div>
          <div class="l-footer_links_menu">
            <ul class="l-footer_links_menu_list02">
              <li><a href="/information/index.html">お知らせ</a></li>
              <!--<li><a href="/recruit/">採用情報</a></li>-->
              <li><a href="/contact/index.html">お問い合わせ</a></li>
              <!-- <li><a href="/sitemap.html">サイトマップ</a></li> -->
            </ul>
          </div>
          <div class="l-footer_links_menu">
            <p class="l-footer_links_menu_ttl">その他</p>
            <ul class="l-footer_links_menu_list">
              <li><a href="/privacy/index.html">個人情報保護方針・個人情報の取り扱いについて</a></li>
              <li><a href="/isms/index.html">情報セキュリティ方針</a></li>
              <li><a href="/ibc/index.html">一般事業主行動計画</a></li>
              <li><a href="/woman-activity/index.html">女性活動推進法に基づく行動計画</a></li>
              <li><a href="/governance/index.html">りらいあグループ行動基準</a></li>
              <li><a href="/sexhara-policy/index.html">セクシャルハラスメント方針</a></li>
              <li><a href="/nenkin/index.html">国民年金保険料の納付のご案内について</a></li>
              <li><a href="/disclosure/index.html">派遣法に基づく情報公開</a></li>
            </ul>
          </div>
        </div>
        <div class="l-footer_links_banner">
          <p class="l-footer_links_banner_item">
            <a href="/nenkin/"><img src="/assets/images/common/banner-nenkin.jpg" alt="アイヴィジットの年金サイト 公的年金についてご説明します。"></a>
          </p>
          <p class="l-footer_links_banner_item">
            <a href="https://www.relia-group.com/" target="_blank"><img src="/assets/images/common/logo-group.jpg" alt="Relia,Inc.GROUP"></a>
          </p>

        </div>
      </div>
      <div class="l-footer_mark">
        <div class="l-footer_mark_left">
          <img src="/assets/images/common/image-pmark.jpg" alt="">
          <p class="l-footer_mark_txt">株式会社アイヴィジットは、「プライバシーマーク」使用許諾事業者として認定されました。</p>
        </div>
        <div class="l-footer_mark_right">
          <img src="/assets/images/common/image-iso.jpg" alt="">
          <p class="l-footer_mark_txt">
            株式会社アイヴィジットは、「ISMS/ISO 27001」の認証取得を致しました。
            <br>IS 561499/ISO 27001　　<br class="u-sp">本社における下記業務で取得しています
            <br>・店頭販売支援 　　<br class="u-sp">・セールスプロモーション支援業務<br>・公共機関／民間企業向けサービス代行業務<br>・営業支援アウトソーシング 　　<br class="u-sp">・人材派遣業
          </p>
        </div>
      </div>
      <small class="l-footer_copy">Copyright &copy;2018 I Visit corp. All rights reserved.</small>
    </div>
    <div class="c-contactBtn_wrapper js-fixedBtn">
      <p class="c-contactBtn"><a href="/contact/index.html"><span>お問い合わせはこちら</span></a></p>
    </div>
  </footer>

