<?php

// pタグ自動挿入解除
remove_filter('the_excerpt', 'wpautop');


/* excerpt変更 */
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_length($length) {
	return 160;
}
add_filter('excerpt_length', 'new_excerpt_length');



/* クォーテーションマークの変換を停止 */
remove_filter('the_title', 'wptexturize'); // 記事のタイトル
remove_filter('the_content', 'wptexturize'); // 記事の本文
remove_filter('comment_text', 'wptexturize'); // コメント欄
remove_filter('the_excerpt', 'wptexturize'); // 抜粋


/* 更新通知を非表示 */
// add_filter( 'pre_site_transient_update_core', '__return_zero' );
// remove_action( 'wp_version_check', 'wp_version_check' );
// remove_action( 'admin_init', '_maybe_update_core' );

// -------------------------------------------------------------------
// lib読み込み
// -------------------------------------------------------------------
require_once ("lib/add-shortcode.php");

define('WP_DEBUG',true);

function add_custom_column( $defaults ) {
  $defaults['permalink'] = 'パーマリンク';
  return $defaults;
}
add_filter('manage_pages_columns', 'add_custom_column');
 
function add_custom_column_id($column_name, $id) {
  if($column_name == 'permalink'){
    echo get_permalink();
  }
}
add_action('manage_pages_custom_column', 'add_custom_column_id', 10, 2);

function wpcf7_main_validation_filter( $result, $tag ) {
  $type = $tag['type'];
  $name = $tag['name'];
  $_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );
  if ( 'email' == $type || 'email*' == $type ) {
    if (preg_match('/(.*)_confirm$/', $name, $matches)){
      $target_name = $matches[1];
      if ($_POST[$name] != $_POST[$target_name]) {
        if (method_exists($result, 'invalidate')) {
          $result->invalidate( $tag,"確認用のメールアドレスが一致していません");
      } else {
          $result['valid'] = false;
          $result['reason'][$name] = '確認用のメールアドレスが一致していません';
        }
      }
    }
  }
  return $result;
}

/*-------------------------------------------*/
/*wp-head不要プログラム削除
/*-------------------------------------------*/
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);

//head内（ヘッダー）絵文字削除 
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles' );
remove_action('admin_print_styles', 'print_emoji_styles');

//head内（ヘッダー）Embed系の記述削除 
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');
remove_action('template_redirect', 'rest_output_link_header', 11 );

?>
