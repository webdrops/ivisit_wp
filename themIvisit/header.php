  <header class="l-header">
    <div class="l-header_inr">
      <?php if ( is_home() || is_front_page() ) : ?>
      <p class="l-header_logo"><a href="/"><img src="/assets/images/common/logo.png" alt="ivisit 株式会社アイヴィジット"></a></p>
      <?php else : ?>
      <h1 class="l-header_logo"><a href="/"><img src="/assets/images/common/logo.png" alt="ivisit 株式会社アイヴィジット"></a></h1>
      <?php endif; ?>
      <div class="l-header_utility">
        <nav class="l-header_utility_nav">
          <ul>
            <li class="js-dropMenu_wrapper">
              <a href="#" class="js-dropMenu_btn">
                <span class="l-header_utility_nav_ttl">会社案内</span>
                <span class="l-header_utility_nav_en">Company Profile</span>
              </a>
              <div class="js-dropMenu">
                <div class="js-dropMenu_inr">
                  <p class="js-dropMenu_ttl"><a href="/company/index.html">会社案内</a></p>
                  <ul class="js-dropMenu_list">
                    <?php 
                    
                    $uri = $_SERVER["REQUEST_URI"];
                    
                    ?>
                    <?php if(strpos($uri,'/company/about') !== false): ?>
                    <li><a href="/company/about.html">会社概要</a></li>
                    <li><a href="#history">沿革</a></li>
                    <li><a href="#group">グループ企業</a></li>
                    <li><a href="/company/branch.html">拠点案内</a></li>
                    <li><a href="/company/philosophy.html">企業理念・品質管理方針</a></li>
                    <?php else: ?>
                    <li><a href="/company/about.html">会社概要</a></li>
                    <li><a href="/company/about.html#history">沿革</a></li>
                    <li><a href="/company/about.html#group">グループ企業</a></li>
                    <li><a href="/company/branch.html">拠点案内</a></li>
                    <li><a href="/company/philosophy.html">企業理念・品質管理方針</a></li>
                    <?php endif; ?>
                  </ul>
                </div>
              </div>
            </li>
            <li class="js-dropMenu_wrapper">
              <a href="#" class="js-dropMenu_btn">
                <span class="l-header_utility_nav_ttl">事業内容</span>
                <span class="l-header_utility_nav_en">Business</span>
              </a>
              <div class="js-dropMenu">
                <div class="js-dropMenu_inr">
                  <p class="js-dropMenu_ttl"><a href="/work/index.html">事業内容</a></p>
                  <ul class="js-dropMenu_list">
                    <li><a href="/work/event.html">イベント企画・運営</a></li>
                    <li><a href="/work/round.html">店舗・販路ラウンド</a></li>
                    <li><a href="/work/varioussurveys.html">各種調査代行・支援</a></li>
                    <li><a href="/work/administration/index.html">行政サービス支援</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="js-dropMenu_wrapper">
              <a href="#" class="js-dropMenu_btn">
                <span class="l-header_utility_nav_ttl">お知らせ</span>
                <span class="l-header_utility_nav_en">News Release</span>
              </a>
              <div class="js-dropMenu">
                <div class="js-dropMenu_inr">
                  <p class="js-dropMenu_ttl"><a href="/information/index.html">お知らせ</a></p>
                  <ul class="js-dropMenu_list">
                  <?php
                $args = array(
                  "post_type" => "post",
                  "posts_per_page" => -1,
                  "post_status" => "publish",
                  "orderby "=> "date",
                );
                $query = new WP_Query($args);
                $postYear = array();
              ?>
              <?php if ($query->have_posts()): ?>
              <?php while ($query->have_posts()) : $query->the_post(); ?>
              <?php array_push($postYear,get_the_date('Y'));?>
              <?php endwhile; ?>
              <?php endif; ?>
              <?php
                $postYearList = array_unique($postYear);
                if(!empty($postYearList)){
                  foreach($postYearList as $value){
                    if(strpos($uri,'/information/index') !== false){
                      echo '<li><a href="#'.$value.'">'.$value.'年</a></li>';
                    }else{
                      echo '<li><a href="/information/index.html#'.$value.'">'.$value.'年</a></li>';
                    }
                  }
                }
              ?>
                  </ul>
                </div>
              </div>
            </li>
            <li>
              <a href="/contact/index.html">
                <span class="l-header_utility_nav_ttl">お問い合わせ</span>
                <span class="l-header_utility_nav_en">Contact</span>
              </a>
            </li>
          </ul>
        </nav>
        <a class="l-header_utility_spBtn js-spMenu_btn">
          <span></span>
          <span></span>
          <span></span>
        </a>
        <p class="l-header_utility_btn">
          <a href="https://jobvisit.net/" target="_blank">
            <span>お仕事探しは</span>
            <img src="/assets/images/common/logo02.png" alt="jobvisit!">
          </a>
        </p>
      </div>
      <div class="l-header_spMenu js-spMenu">
        <div class="l-header_spMenu_inr">
          <div class="l-header_spMenu_lists">
            <ul>
              <li><a href="/company/index.html">会社案内</a></li>
              <li><a href="/work/index.html">事業案内</a></li>
              <li><a href="/information/index.html">お知らせ</a></li>
              <!-- <li><a href="/recruit/">採用情報</a></li> -->
              <li><a href="/contact/index.html">お問い合わせ</a></li>
            </ul>
            <ul>
              <li><a href="/privacy/index.html">個人情報保護方針・個人情報の取り扱いについて</a></li>
              <li><a href="/isms/index.html">情報セキュリティ方針</a></li>
              <li><a href="/ibc/index.html">一般事業主行動計画</a></li>
              <li><a href="/woman-activity/index.html">女性活動推進法に基づく行動計画</a></li>
              <li><a href="/governance/index.html">りらいあグループ行動基準</a></li>
              <li><a href="/sexhara-policy/index.html">セクシャルハラスメント方針</a></li>
              <li><a href="/disclosure/index.html">派遣法に基づく情報公開</a></li>
            </ul>
          </div>
          <p class="l-header_spMenu_banner"><a href="/nenkin/"><img src="/assets/images/common/banner-nenkin.jpg" alt="アイヴィジットの年金サイト 公的年金についてご説明します。"></a></p>
        </div>
      </div>
    </div>
  </header>
