<!-- //▼TOP-CATE▼// -->
<div id="topCate">
<div class="frame">
<h3 class="bsp20"><img alt="事業内容" src="<?php bloginfo('template_directory');?>/common/img/top_ttl_business.png" width="106" height="18" /></h3>
<ul class="clearfix">
<!--****-->
	<li><a href="?page_id=30"><dl>
		<dt><img alt="イメージ：営業・販売支援（店舗・訪問・ネット）" src="<?php bloginfo('template_directory');?>/common/img/top_cate_img04.jpg" width="216" height="80" class="over" /></dt>
		<dd>
			<h4><img alt="営業・販売支援（店舗・訪問・ネット）" src="<?php bloginfo('template_directory');?>/common/img/top_cate_ttl04.png" class="over" /></h4>
			<p>ネットワークやモバイルを活用した効率的な営業支援(B2B・B2C)を提案・ご提供致します。</p>
		</dd>
		</dl></a>
	</li>
<!--****-->
<!--****-->
	<li><a href="?page_id=15"><dl>
		<dt><img alt="イメージ：行政サービス支援（窓口・コール・訪問）" src="<?php bloginfo('template_directory');?>/common/img/top_cate_img01.jpg" width="216" height="80" class="over" /></dt>
		<dd>
			<h4><img alt="行政サービス支援（窓口・コール・訪問）" src="<?php bloginfo('template_directory');?>/common/img/top_cate_ttl01.png" class="over" /></h4>
			<p>これまでの豊富な経験を生かし、行政サービス（窓口・訪問勧奨等）の効率的な支援を行います。</p>
		</dd>
		</dl></a>
	</li>
<!--****-->
<!--****-->
	<li><a href="?page_id=27"><dl>
		<dt><img alt="イメージ：各種調査代行・支援（コール・訪問）" src="<?php bloginfo('template_directory');?>/common/img/top_cate_img03.jpg" width="216" height="80" class="over" /></dt>
		<dd>
			<h4><img alt="各種調査代行・支援（コール・訪問）" src="<?php bloginfo('template_directory');?>/common/img/top_cate_ttl03.png" class="over" /></h4>
			<p>訪問調査や特定地域での調査を効率的に行うための支援を行います。</p>
		</dd>
		</dl></a>
	</li>
<!--****-->
<!--****-->
	<li class="end"><a href="?page_id=18"><dl>
		<dt><img alt="イメージ：イベント企画・運営" src="<?php bloginfo('template_directory');?>/common/img/top_cate_img02.jpg" width="216" height="80" class="over" /></dt>
		<dd>
			<h4><img alt="イベント企画・運営" src="<?php bloginfo('template_directory');?>/common/img/top_cate_ttl02.png" class="over" /></h4>
			<p>イベントの企画・立案～運営までをワンストップで行い、効果的なプロモーションを提供します。</p>
		</dd>
		</dl></a>
	</li>
<!--****-->
</ul>
</div>
</div>
<!-- //△TOP-CATE△// -->