
<?php
// -------------------------------------------------------------------
// ショートコード作成
// -------------------------------------------------------------------

//トップページ、お知らせ出力ショートコード
function postListShortcode() {
  $args = array(
    "post_type" => "post",
    "posts_per_page" => 4,
    "post_status" => "publish"
  );
  $query = new WP_Query($args);
  $html = "";
  if ($query->have_posts()){
    while ($query->have_posts()){
      
      $query->the_post();
      $postID = $query->post->ID;
      $category = get_the_category($postID);
      $categoryName = $category[0]->name;

      $html .= '<li class="c-newsList_item">';
      $html .= '<a href="'.get_the_permalink($postID).'">';
      $html .= '<p class="c-newsList_item_date">'.get_the_time('Y/m/d').'</p>';
      if($categoryName === "人事"){
        $html .= '<p class="c-label">'.$categoryName.'</p>';
      }elseif($categoryName === "入札"){
        $html .= '<p class="c-label c-label--red">'.$categoryName.'</p>';
      }elseif($categoryName === "受託"){
        $html .= '<p class="c-label c-label--blue">'.$categoryName.'</p>';
      }elseif($categoryName === "その他"){
        $html .= '<p class="c-label c-label--gray">'.$categoryName.'</p>';
      }
      $html .= '<p class="c-newsList_item_txt">'.get_the_title($postID).'</p>';
      $html .= '</a>';
      $html .= '</li>';
    }
  return $html;
  }
}

add_shortcode("postList", "postListShortcode");