<?php
/*
Template Name: 固定ページテンプレート(イベント企画・運営)
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#article: http://ogp.me/ns/article#">
<title><?php echo trim(wp_title('', false)); if(wp_title('', false)) { echo ' - '; } bloginfo('name'); ?></title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<meta name="format-detection" content="telephone=no">
<meta name="Keywords" content="営業支援,人材派遣,セールスプロモーション,訪問サービス,アウトソーシング" />
<meta name="Description" content="株式会社アイヴィジットは営業支援・人材派遣企業を官公庁・自治体・百貨店・大手量販店・個人宅といった様々なフィールドオペレーションに支援サービスをご提供します" />
<link rel="shortcut icon" href="<?php home_url(); ?>/favicon.ico">
<link rel="stylesheet" href="/assets/css/master.css">
<link rel="stylesheet" href="/assets/css/slick.css">
<link rel="stylesheet" href="/assets/css/swiper.min.css">
<?php wp_head(); ?>
<script src="/assets/js/vendor/jquery.1.11.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHLWZ8C');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div id="l-wrapper">
<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

<?php
	remove_filter('the_content', 'wpautop');
	the_content();
	add_filter('the_content', 'wpautop');
 ?>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
<div class="c-modalOverlay js-modalOverlay js-modalClose">
  </div>
  <div class="c-modal js-modal" id="modal01">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：大手都市ガス事業者</h3>
          <p class="c-modal_heading_desc_txt">＜電力参入に伴う、認知度向上イベントの企画運営＞</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/event/image-modal01-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">既存顧客囲い込みの為、電力サービスを開始したもののPOPやDM、既存イベントでのお知らせだけでは中々認知度が上がらず、契約数も目標を割り込んでいました。</p>
      <p class="c-modal_txt">そこで類似業務の実績が豊富なIvisitに相談します。</p>
      <div class="c-modal_footer">
        <figure class="c-modal_footer_img">
          <img src="/assets/images/work/event/image-modal01-2.jpg" alt="">
        </figure>
        <div class="c-modal_footer_desc">
          <p class="c-modal_footer_desc_txt">適切なアプローチの仕方（企画）から共に考えサービスを訴求するイベント運営を計画しました。地域住民の方へのイベントの事前周知と当日の運営をIvisitが担当し、見込み顧客を発掘。その後のクロージングをクライアント企業が担い、順調に契約数を伸ばしていきました。</p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal01 -->
  <div class="c-modal js-modal" id="modal02">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：大手ドリンクメーカー</h3>
          <p class="c-modal_heading_desc_txt">＜新製品発売時の試飲体感イベント＞</p>
          <p class="c-modal_heading_desc_txt">新製品の特徴が飲まないと伝わり難いものだった為試飲PRを全国で一斉に開催する事を決定し、Ivisitに相談。<br>打合せの結果、3つの課題をクリアする必要性がありました。
          </p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/event/image-modal02-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">①試飲を通して、新製品の特長をPRし認知度を向上。<br>②ただ体感してもらうだけでなく、売上拡大に繋げたい。<br>③ぎりぎりまでイベント会場の調整がかかる為、短いリードタイムで確実に運営したい。</p>
      <div class="c-modal_footer">
        <figure class="c-modal_footer_img">
          <img src="/assets/images/work/event/image-modal02-2.jpg" alt="">
        </figure>
        <div class="c-modal_footer_desc">
          <p class="c-modal_footer_desc_txt">Ivisitは事務局を中心とした対応体制を敷き、延べ400名を準備。欠員無くイベントを開催しました。</p>
          <p class="c-modal_footer_desc_txt">①事務局が研修を運営手法と研修を統括し、各地の情報を集約し豊富な根拠からPDCAサイクルによりよりお客様に訴求する運営を短い期間で運営。<br>②店頭販売支援のノウハウを活かし、リーダーがスタッフに販売に繋げるトークを徹底。<br>③各地に予備人員を確保し、確実な運営を実施。</p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal02 -->
  <div class="c-modal js-modal" id="modal03">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：自治体、大手印刷会社（スポンサー企業）</h3>
          <p class="c-modal_heading_desc_txt">＜世界的スポーツ大会に向けた次世代興味付イベント＞</p>
          <p class="c-modal_heading_desc_txt">世界的スポーツイベント開催を控えた自治体から、スポンサー企業である印刷会社に、次世代の子供たちにイベントの素晴らしさを伝える取組の相談が入ります。早速協力会社であるIvisitに相談し、共に取り組んでいく事を決めました。</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/event/image-modal03-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">＜内容＞<br>地域内の学校と調整し、過去の世界的スポーツ大会の出場アスリートと子供たちが直接交流する取組を延べ315校で実施。</p>
      <div class="c-modal_footer">
        <figure class="c-modal_footer_img">
          <img src="/assets/images/work/event/image-modal03-2.jpg" alt="">
        </figure>
        <div class="c-modal_footer_desc">
          <p class="c-modal_footer_desc_txt">・実施校との事前打ち合わせ<br>
            ・実施計画書の作成<br>
            ・アスリートとの事前打ち合わせ<br>
            ・当日使用する資材の準備、発送<br>
            ・授業当日のサポート<br>
            （ディレクター1名、アシスタントディレクター1～2名）<br>
            ・実施報告書の作成<br>
            等を担当し、学校からも高い評価を得ました。
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal03 -->
  <div class="c-modal js-modal" id="modal04">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：プロ野球球団
          </h3>
          <p class="c-modal_heading_desc_txt">＜ファンクラブの会員獲得イベント＞</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/event/image-modal04-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">球界の活性化と本拠地の観戦者の増加を目的としてホーム試合の際に球場敷地内でファンクラブの会員獲得を実施。新規会員数が伸び悩んでいた為、Ivisitに相談しました。</p>
      <div class="c-modal_footer">
        <figure class="c-modal_footer_img">
          <img src="/assets/images/work/event/image-modal04-2.jpg" alt="">
        </figure>
        <div class="c-modal_footer_desc">
          <p class="c-modal_footer_desc_txt">Ivisitは現場ディレクターを中心にイベント運営をしながら、改善点をお客様と協議。<br>
            主に<br>
            ・ブースの見せ方の改善<br>
            ・スタッフ毎の役割の明確化（分業制）<br>
            ・積極的な声だしとその内容の工夫<br>
            等を行い、目標会員数をクリアしました。
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal04 -->
  <div class="c-modal js-modal" id="modal05">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：医療機器メーカー
          </h3>
          <p class="c-modal_heading_desc_txt">＜医学会（展示会）におけるブース案内＞</p>
          <p class="c-modal_heading_desc_txt">取引先の医療機器メーカーは、自社製品の売上向上の為に医学会にブースを出す事を決めましたが、いくつか課題を抱えていました。</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/event/image-modal05-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">・自社の社員だけでは人手が不足している。<br>
        ・来場者の多くは医師であり、対応を間違えるとその後大きな問題に発展する可能性がある為、応対するスタッフには質の高い応対接遇スキルが求められる。<br>
        ・展示会で取り扱う機器が特殊な為、機器の知識、取り扱いを素早く習得できる人材を用意する必要がある。
      </p>
      <p class="c-modal_txt">Ivisitは来場者の特性に合わせた人材を用意しました。</p>
      <div class="c-modal_footer">
        <figure class="c-modal_footer_img">
          <img src="/assets/images/work/event/image-modal05-2.jpg" alt="">
        </figure>
        <div class="c-modal_footer_desc">
          <p class="c-modal_footer_desc_txt">・即戦力として類似業務経験のある人材をアサイン。<br>
            ・品質管理専門部署が作成した応対マニュアルによる接遇研修を実施。挨拶、服装、お辞儀の角度等、<br>
            基本的スキルを身に付けさせ、基本的応対力を強化等を行い、お客様企業の目的を達成する事が出来ました。
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal05 -->
</div>
<script src="/assets/js/vendor/jquery.matchHeight.js"></script>
<script src="/assets/js/vendor/picturefill.min.js"></script>
<script src="/assets/js/vendor/ofi.min.js"></script>
<script src="/assets/js/vendor/slick.min.js"></script>
<script src="/assets/js/vendor/swiper.min.js"></script>
<script src="/assets/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>