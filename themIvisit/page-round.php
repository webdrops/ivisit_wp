<?php
/*
Template Name: 固定ページテンプレート(店舗・販路ラウンド)
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#article: http://ogp.me/ns/article#">
<title><?php echo trim(wp_title('', false)); if(wp_title('', false)) { echo ' - '; } bloginfo('name'); ?></title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<meta name="format-detection" content="telephone=no">
<meta name="Keywords" content="営業支援,人材派遣,セールスプロモーション,訪問サービス,アウトソーシング" />
<meta name="Description" content="株式会社アイヴィジットは営業支援・人材派遣企業を官公庁・自治体・百貨店・大手量販店・個人宅といった様々なフィールドオペレーションに支援サービスをご提供します" />
<link rel="shortcut icon" href="<?php home_url(); ?>/favicon.ico">
<link rel="stylesheet" href="/assets/css/master.css">
<link rel="stylesheet" href="/assets/css/slick.css">
<link rel="stylesheet" href="/assets/css/swiper.min.css">
<?php wp_head(); ?>
<script src="/assets/js/vendor/jquery.1.11.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHLWZ8C');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div id="l-wrapper">
<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

<?php
	remove_filter('the_content', 'wpautop');
	the_content();
	add_filter('the_content', 'wpautop');
 ?>

	<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
<div class="c-modalOverlay js-modalOverlay js-modalClose">
  </div>
  <div class="c-modal js-modal" id="modal01">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：スマートフォンメーカー</h3>
          <p class="c-modal_heading_desc_txt">＜店舗巡回による販売支援＞</p>
          <p class="c-modal_heading_desc_txt">定期的に新製品が発売となり、他社メーカーとの差別化も難しくなってきた業界において、ストアシェアを伸ばす為に以下の課題がありました。</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/round/image-modal01-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">・店舗販売員の顧客対応力を引き上げ<br>
        ・新商品の機能・訴求ポイントを店舗販売員に習得してもらう<br>
        ・販売店に商品知識・操作方法を浸透させる<br>
        ・来店顧客の要望・意見を吸い上げ
      </p>
      <p class="c-modal_txt">Ivisitは事務局を中心とした店舗巡回型の営業スタッフを配置し<br>
        ・お取引様からの問合せ対応窓口の設置<br>
        ・全店舗の売場カルテ作り
      </p>
      <p class="c-modal_txt">①新商品のレクチャー（勉強会）を期間内に完了する為<br>
        進捗状況を日々管理、ラウンダーのシフト変更を実施<br>
        ②ポジティブ・ネガティブ意見収集及び分析<br>
        トークスクリプトやFAQの作成・展開・更新<br>
        ③業務報告システムの導入による日次の声の吸い上げ
      </p>
      <p class="c-modal_txt">等を実行。今ではお客様企業にとって欠かせない機能となっています。</p>
    </div>
  </div>
  <!-- /.modal01 -->
  <div class="c-modal js-modal" id="modal02">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：コンテンツサービス事業者</h3>
          <p class="c-modal_heading_desc_txt">＜店舗巡回によるプリペイドカード販売支援＞</p>
          <p class="c-modal_heading_desc_txt">プリペイドカードを使ったコンテンツサービスを配信していましたが、店舗メンテナンスが不十分で機会損失していたり、什器設置交渉や提案が十分に出来ていない等の課題がありました。</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/round/image-modal02-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">そこでラウンド業務の実績が豊富なIvisitに相談する事にしました。</p>
      <p class="c-modal_txt">Ivisitは事務局を中心とした店舗巡回型の営業スタッフを配置し<br>
        ①陳列/清掃作業を確実に履行するための独自チェックリストによる運用<br>
        ②スマートフォン、Web報告システムを使うことで、各ラウンダーのノウハウ・ナレッジ(成功/失敗事例)を標準化<br>
        ③店舗に対する交渉/プロモーション提案確度を向上させるために、他店舗での実績例を積極紹介<br></p>
      <p class="c-modal_txt">等を行い、昨対売上6%向上、什器設置数平均2.3箇所/店増という実績を出し、評価頂きました。</p>
    </div>
  </div>
  <!-- /.modal02 -->
  <div class="c-modal js-modal" id="modal03">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：コンテンツサービス事業者</h3>
          <p class="c-modal_heading_desc_txt">＜既存顧客店舗に対する巡回訪問フォロー ＞</p>
          <p class="c-modal_heading_desc_txt">飲食店向けの予約システムを提供していましたが、店舗側が多忙とリテラシーの低さから有効活用されておらず、解約に繋がっていました。現状を打開すべく、Ivisitに相談しました。</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/round/image-modal02-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">Ivisitは店舗巡回し、店舗毎の実態に即した利用法法の説明、アドバイスを行いました。<br>
        また、巡回時に以下の点を工夫。</p>
      <p class="c-modal_txt">①訪問時間の設定を工夫し、面談率を向上<br>
        ②タブレットを活用し、視覚的に訴求し理解度を深める<br>
        ③利用状況や、使い勝手等をヒアリングし、今後のコンテンツ開発の材料とする。</p>
      <p class="c-modal_txt">結果、解約防止に繋がると共に店舗の売上の拡大にも貢献しまさに「WIN＝WIN関係の創出」となり、評価を頂きました。</p>
    </div>
  </div>
  <!-- /.modal03 -->
  <div class="c-modal js-modal" id="modal04">
    <div class="c-modal_close"><a href="#" class="js-modalClose"><img src="/assets/images/common/icon-close.png" alt=""></a></div>
    <div class="c-modal_inr">
      <div class="c-modal_heading">
        <div class="c-modal_heading_desc">
          <h3 class="c-secTtl05">お客様企業：介護事業システムベンダー</h3>
          <p class="c-modal_heading_desc_txt">＜介護事業巡回による顧客フォロー ＞</p>
          <p class="c-modal_heading_desc_txt">介護事業者向けの決済複合システムを提供していましたが、多機能性が仇となり、保険請求機能のみ利用されていて他の機能が活用されていないという実態になっていました。</p>
        </div>
        <figure class="c-modal_heading_img">
          <img src="/assets/images/work/round/image-modal04-1.jpg" alt="">
        </figure>
      </div>
      <p class="c-modal_txt">本システムの特長が生かされずリプレースに繋がっていた為、Ivisitに顧客フォローの相談が入ります。</p>
      <p class="c-modal_txt">Ivisitは既存契約介護事業者を訪問し、システムの有効利用の為に以下の点を工夫し実演案内を実施しました。</p>
      <p class="c-modal_txt">①タブレットを活用し、視覚的に訴求し理解度を深める<br>
        ②対話中にニーズの在り処を把握し、活用見込みの高い機能を選択してご案内する<br>
        ③中小事業者はアポイント無しでの訪問、規模の大きい医療法人は電話アポを取得し、面談率を高める</p>
      <p class="c-modal_txt">結果、システムの機能の有効性を実感頂いた事業者はロイヤリティが高まり、自然と業界に横展開される環境が醸成されました。</p>
    </div>
  </div>
  <!-- /.modal04 -->
</div>
<script src="/assets/js/vendor/jquery.matchHeight.js"></script>
<script src="/assets/js/vendor/picturefill.min.js"></script>
<script src="/assets/js/vendor/ofi.min.js"></script>
<script src="/assets/js/vendor/slick.min.js"></script>
<script src="/assets/js/vendor/swiper.min.js"></script>
<script src="/assets/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>