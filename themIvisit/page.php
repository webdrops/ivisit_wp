<?php
/*
Template Name: 固定ページテンプレート
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#article: http://ogp.me/ns/article#">
<title><?php echo trim(wp_title('', false)); if(wp_title('', false)) { echo ' - '; } bloginfo('name'); ?></title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<meta name="format-detection" content="telephone=no">
<meta name="Keywords" content="営業支援,人材派遣,セールスプロモーション,訪問サービス,アウトソーシング" />
<meta name="Description" content="株式会社アイヴィジットは営業支援・人材派遣企業を官公庁・自治体・百貨店・大手量販店・個人宅といった様々なフィールドオペレーションに支援サービスをご提供します" />
<link rel="shortcut icon" href="<?php home_url(); ?>/favicon.ico">
<link rel="stylesheet" href="/assets/css/master.css">
<link rel="stylesheet" href="/assets/css/slick.css">
<link rel="stylesheet" href="/assets/css/swiper.min.css">
<?php wp_head(); ?>
<script src="/assets/js/vendor/jquery.1.11.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHLWZ8C');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div id="l-wrapper">
<?php get_header(); ?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

<?php
	remove_filter('the_content', 'wpautop');
	the_content();
	add_filter('the_content', 'wpautop');
 ?>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
</div>
<script src="/assets/js/vendor/jquery.matchHeight.js"></script>
<script src="/assets/js/vendor/picturefill.min.js"></script>
<script src="/assets/js/vendor/ofi.min.js"></script>
<script src="/assets/js/vendor/slick.min.js"></script>
<script src="/assets/js/vendor/swiper.min.js"></script>
<script src="/assets/js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>