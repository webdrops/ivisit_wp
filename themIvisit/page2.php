<?php
/*
Template Name: 2階層目テンプレート
*/
?>


<?php get_header(); ?>


<?php if(!is_front_page()) { ?>
<!-- //▼PAN▼// -->
<div id="pan">
<div class="frame">
<!--{*
<ul class="clearfix">
	<li><a href="<?php bloginfo('url'); ?>">ホーム</a>&gt;</li>
	<li><?php the_title(); ?></li>
</ul>
*}-->
<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>
</div>
</div>
<!-- //△PAN△// -->
<?php } ?>


<?php if(is_page( array(15,18,27,30))) { ?>
<?php get_template_part( 'jigyou-menu' ); ?>
<?php } ?>


<!-- //▼CONTAINER▼// -->
<div id="container">
<div class="frame clearfix">


<!-- //▼MAIN▼// -->
<div id="mainArea">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

<?php
	remove_filter('the_content', 'wpautop');
	the_content();
	add_filter('the_content', 'wpautop');
 ?>

	<?php endwhile; ?>
<?php endif; ?>
</div>
<!-- //△MAIN△// -->

<!-- //▼SIDE▼// -->
<div id="sideArea">
<?php get_sidebar(); ?>
</div>
<!-- //△SIDE△// -->


</div>
</div>
<!-- //△CONTAINER△// -->



<?php get_footer(); ?>
