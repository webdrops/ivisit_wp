<?php if(!is_front_page()) { ?>
<!--****-->
<div id="sideMenu">
<h4><img alt="事業内容" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_business.png" width="260" height="40" /></h4>
<ul>
	<li><a href="?page_id=30">営業･販売支援(店舗･訪問･ネット)</a></li>
	<li><a href="?page_id=15">行政サービス支援(窓口･コール･訪問)</a></li>
	<li><a href="?page_id=27">各種調査代行･支援(コール･訪問)</a></li>
	<li><a href="?page_id=18">イベント企画･運営</a></li>
</ul>
<h4><img alt="入札実績" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_nyusatsu.png" width="260" height="39" /></h4>
<ul>
	<li><a href="?page_id=840">平成29年度受注実績</a></li>
	<li><a href="?page_id=840">平成28年度受注実績</a></li>
</ul>
<h4><img alt="会社案内" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_about.png" width="260" height="39" /></h4>
<ul>
	<li><a href="?page_id=38">会社概要</a></li>
	<li><a href="?page_id=38#enkaku">沿　革</a></li>
	<!--<li><a href="?page_id=41">代表メッセージ</a></li>-->
	<li><a href="?page_id=44">拠点案内</a></li>
	<li><a href="?page_id=88">お知らせ一覧</a></li>
	<li><a href="?page_id=368">企業理念・品質管理方針</a></li>
</ul>
<h4><img alt="お客様企業お問合せ" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_client.png" width="260" height="39" /></h4>
<ul>
	<li><a href="https://www.ivisit.co.jp/client/index.php">お客様企業お問い合わせ</a></li>
</ul>
<!--{*
<h4><img alt="お仕事をご希望の皆さま" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_staff.png" width="260" height="39" /></h4>
<ul>
	<li><a href="?page_id=59">お仕事までの流れ</a></li>
	<li><a href="/staff/form.php">オンライン仮登録</a></li>
</ul>
*}-->
</div>
<!--****-->
<?php } ?>
<!--****-->
<p id="btnJobvisit" class="bsp20"><a href="http://jobvisit.net/" target="_blank"><img alt="販売・官公庁のアルバイト・バイト求人ならジョブヴィジット" src="<?php bloginfo('template_directory');?>/common/img/ban_jobvisit.png" width="260" height="144" class="over" /><span>販売・官公庁のアルバイト・<br />バイト求人ならジョブヴィジット</span></a></p>
<!--****-->
<!--****-->
<p class="bsp30"><a href="?page_id=193"><img alt="国民年金保険料の納付のご案内" src="<?php bloginfo('template_directory');?>/common/img/ban_nenkin.png" width="260" height="80" class="over" /></a></p>
<!--****-->
<!--****-->
<div id="foothold">
<h4><a href="?page_id=44#honsya_ttl"><img alt="本社・支店" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_honten.png" width="260" height="40" /></a></h4>
<ul id="sfOffice">
	<li><a href="?page_id=44#honsya">本社 （東京都渋谷区）</a></li>
	<li><a href="?page_id=44#tohoku">東北<!--支社-->支店 （宮城県仙台市）</a></li>
	<li><a href="?page_id=44#kansai">関西<!--支社-->支店 （大阪府大阪市）</a></li>
	<li><a href="?page_id=44#chubu">中部支店 （愛知県名古屋市）</a></li>
	<li><a href="?page_id=44#chushikoku">中国・四国支店 （広島県広島市）</a></li>
	<li><a href="?page_id=44#kyushu">九州支店 （福岡県福岡市）</a></li>
</ul>
<h4><a href="?page_id=44#kyoten_ttl"><img alt="全国の事業拠点" src="<?php bloginfo('template_directory');?>/common/img/side_ttl_foothold.png" width="260" height="30" /></a></h4>
<ul id="sfArea" class="clearfix">
	<li><a href="?page_id=44#ootuka_ce">大塚ハイブリッドセンター</a></li>
	<li><a href="?page_id=44#osaka_ce">大阪センター</a></li>
	<li><a href="?page_id=44#sapporo">札幌</a></li>
	<li><a href="?page_id=44#sapporo3">札幌第三</a></li>
	<!--<li><a href="?page_id=44#morioka">盛岡</a></li>-->
	<li><a href="?page_id=44#sendai2">仙台第二</a></li>
	<!--<li><a href="?page_id=44#nigata">新潟</a></li>-->
	<li><a href="?page_id=44#nigata2">新潟第二</a></li>
	<li><a href="?page_id=44#takasaki">高崎</a></li>
	<!--<li><a href="?page_id=44#mito">水戸</a></li>-->
	<li><a href="?page_id=44#saitama">さいたま</a></li>
	<!--<li><a href="?page_id=44#shinjuku">新宿</a></li>-->
    <li><a href="?page_id=44#yoyogi">代々木</a></li>
	<li><a href="?page_id=44#yokohama">横浜</a></li>
	<!--<li><a href="?page_id=44#makuhari">幕張</a></li>-->
	<!--<li><a href="?page_id=44#kanazawa">金沢</a></li>-->
	<li><a href="?page_id=44#shizuoka">静岡</a></li>
	<li><a href="?page_id=44#hamamatu">浜松</a></li>
	<!--<li><a href="?page_id=44#yokkaichi">四日市</a></li>
	<li><a href="?page_id=44#suzuka">鈴鹿</a></li>
	<li><a href="?page_id=44#tsu">津</a></li>
	<li><a href="?page_id=44#iga">伊賀</a></li>
	<li><a href="?page_id=44#matsusaka">松阪</a></li>
	<li><a href="?page_id=44#owase">尾鷲</a></li>-->
	<li><a href="?page_id=44#osaka">大阪</a></li>
	<!--<li><a href="?page_id=44#nara">奈良</a></li>-->
	<li><a href="?page_id=44#okayama">岡山</a></li>
	<!--<li><a href="?page_id=44#hiroshima">広島第二</a></li>-->
	<li><a href="?page_id=44#kitakyusyu">北九州</a></li>
	<!-- <li><a href="?page_id=44#kurume">久留米</a></li> -->
	<li><a href="?page_id=44#iizuka">飯塚</a></li>
	<li><a href="?page_id=44#ooita">大分</a></li>
	<li><a href="?page_id=44#kumamoto">熊本</a></li>
	<!--<li><a href="?page_id=44#kagoshima">鹿児島</a></li>-->
	<!--<li><a href="?page_id=44#naha">那覇</a></li>-->
</ul>
</div>
<!--****-->
