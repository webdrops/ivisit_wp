<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#article: http://ogp.me/ns/article#">
<title><?php the_title(); ?> - 営業支援/人材派遣アウトソーシングはアイヴィジット</title>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initialscale=1">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="株式会社アイヴィジットは営業支援・人材派遣企業を官公庁・自治体・百貨店・大手量販店・個人宅といった様々なフィールドオペレーションに支援サービスをご提供します">
<meta name="keyword" content="営業支援,人材派遣,セールスプロモーション,訪問サービス,アウトソーシング">
<link rel="shortcut icon" href="<?php home_url(); ?>/favicon.ico">
<link rel="stylesheet" href="/assets/css/master.css">
<link rel="stylesheet" href="/assets/css/slick.css">
<link rel="stylesheet" href="/assets/css/swiper.min.css">
<link rel="stylesheet" href="/assets/css/import.css">
<script src="/assets/js/vendor/jquery.1.11.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHLWZ8C');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<div id="l-wrapper">
  <?php get_header(); ?>
  <article>
    <div class="c-pageTtl_wrapper">
      <h1 class="c-pageTtl c-pageTtl--information">
        <span class="c-pageTtl_ttl">お知らせ</span>
        <span class="c-pageTtl_en">News Release</span>
      </h1>
    </div>
    <div class="l-contents">
      <section class="l-sec02">
        <div class="l-wrap">
          <h2 class="c-secTtl03 u-mb15"><?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?><?php the_title(); ?><?php endwhile; ?><?php endif; ?></h2>
          <p class="u-tar u-textSmall u-mb30 u-mb10_sp"><?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?><?php the_time('Y年m月d日'); ?><?php endwhile; ?><?php endif; ?></p>
          <div id="information">
          <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>

          <?php
            remove_filter('the_content', 'wpautop');
            the_content();
            add_filter('the_content', 'wpautop');
          ?>

          	<?php endwhile; ?>
          <?php endif; ?>
            
          </div>
          <p class="c-btn c-btn--trans u-mt80 u-mt40_sp"><a href="/information/index.html">お知らせ一覧に戻る</a></p>
        </div>
      </section>
    </div>
  </article>
  <?php get_footer(); ?>
</div>
<script src="/assets/js/vendor/jquery.matchHeight.js"></script>
<script src="/assets/js/vendor/picturefill.min.js"></script>
<script src="/assets/js/vendor/ofi.min.js"></script>
<script src="/assets/js/vendor/slick.min.js"></script>
<script src="/assets/js/vendor/swiper.min.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
